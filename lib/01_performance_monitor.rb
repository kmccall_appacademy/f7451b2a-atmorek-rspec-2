require "time"

def measure(reps = 1, &prc)
  times = []

  reps.times do
    t = Time.now
    prc.call
    times << Time.now - t
  end

  times.reduce(0, :+) / times.length
end
